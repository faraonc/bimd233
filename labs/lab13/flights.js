"use strict";
/*
Create a mock flight reservation system that will create 5 flight objects
according to the following properties:  airline, number, origin, destination,
dep_time, arrival_time, arrival_gate. Add a method to return the flight
duration (gate to gate).
*/
function msToTime(duration) {
	var milliseconds = parseInt((duration%1000)/100)
	, seconds = parseInt((duration/1000)%60)
	, minutes = parseInt((duration/(1000*60))%60)
	, hours = parseInt((duration/(1000*60*60))%24);

	hours = (hours < 10) ? "0" + hours : hours;
	minutes = (minutes < 10) ? "0" + minutes : minutes;
	seconds = (seconds < 10) ? "0" + seconds : seconds;

	return hours + ":" + minutes + ":" + seconds;
}

function Flight(airline, number, origin, destination, dep_time, arrival_time, arrival_gate){
	this.airline = airline;
	this.number = number;
	this.origin = origin;
	this.destination = destination;
	this.dep_time = new Date(dep_time);
	this.arrival_time = new Date(arrival_time);
	this.arrival_gate = arrival_gate;
	this.getFlightDuration = function(){
		var diff = this.arrival_time.getTime() - this.dep_time.getTime();
		return msToTime(diff);
	}
}

var flight0 = new Flight(
	"Alaska Airlines", 
	"ASA397", 
	"SAN", 
	"SEA" ,
	"Wed November 15, 2017 17:04:10 GMT-0700 (PDT)",
	"Wed November 15, 2017 19:45:13 GMT-0700 (PDT)",
	"C9"
	);

var flight1 = new Flight(
	"Compass Airlines", 
	"CPZ5711", 
	"SAN", 
	"SEA" ,
	"Wed November 15, 2017 11:20:08 GMT-0700 (PDT)",
	"Wed November 15, 2017 13:59:25 GMT-0700 (PDT)",
	"B9A"
	);

var flight2 = new Flight(
	"Skywest", 
	"SKW3475", 
	"PDX", 
	"SEA" ,
	"Wed November 15, 2017 22:30:02 GMT-0700 (PDT)",
	"Wed November 15, 2017 23:06:11 GMT-0700 (PDT)",
	"C9"
	);

var flight3 = new Flight(
	"United", 
	"UAL351", 
	"SFO", 
	"SEA" ,
	"Wed November 15, 2017 22:55:28 GMT-0700 (PDT)",
	"Thu November 16, 2017 01:08:32 GMT-0700 (PDT)",
	"A11"
	);

var flight4 = new Flight(
	"Virgin America", 
	"VRD1797", 
	"LAX", 
	"SEA" ,
	"Wed November 15, 2017 21:45:41 GMT-0700 (PDT)",
	"Thu November 16, 2017 00:12:38 GMT-0700 (PDT)",
	"A7"
	);

var flights = [flight0,flight1,flight2,flight3,flight4];

var tableRef = document.getElementById("flight-table");
const HEADER_LENGTH = 8
for(var i = 0; i < flights.length; i++)
{
	var j = 0;
	var newRow   = tableRef.insertRow(tableRef.rows.length);

	var newCell  = newRow.insertCell(j);
	newCell.textContent = flights[i].airline;
	j++;

	var newCell  = newRow.insertCell(j);
	newCell.textContent = flights[i].number;
	j++;

	var newCell  = newRow.insertCell(j);
	newCell.textContent = flights[i].origin;
	j++;

	var newCell  = newRow.insertCell(j);
	newCell.textContent = flights[i].dep_time;
	j++;

	var newCell  = newRow.insertCell(j);
	newCell.textContent = flights[i].destination;
	j++;

	var newCell  = newRow.insertCell(j);
	newCell.textContent = flights[i].arrival_time;
	j++;

	var newCell  = newRow.insertCell(j);
	newCell.textContent = flights[i].getFlightDuration();
	j++;

	var newCell  = newRow.insertCell(j);
	newCell.textContent = flights[i].arrival_gate;
	j++;
};


for(var i = 0; i < flights.length; i++)
{
	console.log(flights[i].getFlightDuration())
}



