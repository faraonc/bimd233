
// Given the scorekeeper starter html, css and js code from your Week10 Canvas files, add the following behavior: 
// 1) Modify the “change” event to not allow negative number entries.  
// 2) Using Bootstrap Button (Links to an external site.)Links to an external site. classes, 
// change the default style of the buttons to “Default” except for when the Player “wins” in
//  which case the winner’s button shall be changed to the “success” style.

var p1Button = document.querySelector("#p1");
var p2Button = document.getElementById("p2");
p1Button.classList.add("btn", "btn-default");
p2Button.classList.add("btn", "btn-default");
var resetButton = document.getElementById("reset");
resetButton.classList.add("btn", "btn-default");
var p1Display = document.querySelector("#p1Display");
var p2Display = document.querySelector("#p2Display");
var numInput = document.querySelector("input");
numInput.setAttribute("min", "1");
var winningScoreDisplay = document.querySelector("p span");
var p1Score = 0;
var p2Score = 0;
var gameOver = false;
var winningScore = 5;

p1Button.addEventListener("click", function() {
  if (!gameOver) {
    p1Score++;
    if (p1Score === winningScore) {
      p1Display.classList.add("winner");
      p1Button.classList.add("btn-success");
      gameOver = true;
    }
    p1Display.textContent = p1Score;
  }
});

p2Button.addEventListener("click", function() {
  if (!gameOver) {
    p2Score++;
    if (p2Score === winningScore) {
      p2Display.classList.add("winner");
      p2Button.classList.add("btn-success");
      gameOver = true;
    }
    p2Display.textContent = p2Score;
  }
});

resetButton.addEventListener("click", function() {
  reset();
});

function reset() {
  p1Score = 0;
  p2Score = 0;
  p1Display.textContent = 0;
  p2Display.textContent = 0;
  p1Display.classList.remove("winner");
  p2Display.classList.remove("winner");
  p1Button.classList.remove("btn-success");
  p2Button.classList.remove("btn-success");
  gameOver = false;
}

numInput.addEventListener("change", function() {
    if(this.value <= 0){
      this.value = 1;
    }
    winningScoreDisplay.textContent = this.value;
    winningScore = Number(this.value);
    reset();
  
});