
// Given the todo list starter html, css and js code from your Week10 Canvas files, add the following behavior:
//  1) using the mouseover event, change the style from deselected to selected and 
//  2) the text in the todo list item to “Focused!”.
//var todos = document.querySelector("ul");

var lis = document.querySelectorAll("li");
var focusedItem = lis[0];

for(var i = 0; i < lis.length; i++){
	//console.log(this.id);
	lis[i].addEventListener("mouseover", function(){
		console.log("mouseover");
		this.setAttribute("id", "selected");
	});

	lis[i].addEventListener("mouseout", function(){
		console.log("mouseout");
		this.setAttribute("id", "deselected");
	});

	/************************************************************************/
	//Focus means you have selected the particular GUI element not mouse over!
	/***********************************************************************/
	lis[i].addEventListener("click", function(){
		console.log("clicked");
		if(this.firstChild.nodeValue !== "Focused!"){
			focusedItem.firstChild.nodeValue = "Not focused";
			focusedItem = this;
			focusedItem.firstChild.nodeValue = "Focused!";
		}
	});
}

var element = document.getElementById("selected");
if(element){
	element.setAttribute("id", "deselected");
}

