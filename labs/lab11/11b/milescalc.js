

//source: https://www.qodo.co.uk/blog/javascript-checking-if-a-date-is-valid/

// Checks a string to see if it in a valid date format
// of (D)D/(M)M/(YY)YY and returns true/false
function isValidDate(s) {
    // format D(D)/M(M)/(YY)YY
    var dateFormat = /^\d{1,4}[\.|\/|-]\d{1,2}[\.|\/|-]\d{1,4}$/;

    if (dateFormat.test(s)) {
        // remove any leading zeros from date values
        s = s.replace(/0*(\d*)/gi,"$1");
        var dateArray = s.split(/[\.|\/|-]/);

              // correct month value
              dateArray[1] = dateArray[1]-1;

        // correct year value
        if (dateArray[2].length<4) {
            // correct year value
            dateArray[2] = (parseInt(dateArray[2]) < 50) ? 2000 + parseInt(dateArray[2]) : 1900 + parseInt(dateArray[2]);
        }

        var testDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
        if (testDate.getDate()!=dateArray[0] || testDate.getMonth()!=dateArray[1] || testDate.getFullYear()!=dateArray[2]) {
        	return false;
        } else {
        	return true;
        }
    } else {
    	return false;
    }
}

function isNumeric(num){
	return !isNaN(num)
}

var carName = prompt("Enter your car name: ");
console.log("car name = " + carName);

do
{
	var taxDate = prompt("Enter date (D)D/(M)M/(YY)YY : ");
	console.log("date = " + taxDate);
}while(!isValidDate(taxDate))

do
{
	var milesDriven = prompt("Enter miles driven (positive number only) : ");
	console.log("miles driven = " + milesDriven);
}while(!isNumeric(milesDriven) || milesDriven <= 0)

var deduction = milesDriven * 0.57;
console.log("tax deduction = " + deduction);


var el = document.getElementById("tax-deduction");
el.textContent = "Deduction:  $ " + deduction;
