
// Fix the JS so multiple clicks will not add additional list items.  
// Add the “weather” and “wind” indication on to your ajax-wx sample files to callout the type of weather there is and 
// the string used to describe the current wind conditions

// Shorthand for $( document ).ready()
$(function() {
  // weather update button click
  $('button').on('click', function(e) {
    $(this).remove();
    $('ul li').each(function() {
      console.log("this:" + this);
      $(this).remove();
    });
    $.ajax({
      url: "http://api.wunderground.com/api/7542a9a678392c2f/geolookup/conditions/q/WA/Bothell.json",
      dataType: "jsonp",
      success: function(parsed_json) {
        var city = parsed_json['location']['city'];
        var state = parsed_json['location']['state'];
        var temp_f = parsed_json['current_observation']['temp_f'];
        var rh = parsed_json['current_observation']['relative_humidity'];
        var weather = parsed_json['current_observation']['weather'];
        var wind = parsed_json['current_observation']['wind_string'];

        //city
        var str = "<li> Location : " + city + ", " + state + "</li>";
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');

        //temperature
        str = "<li> Temperature: " + temp_f + " deg F</li>";
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');

        //humidity
        str = "<li> Relative Humidity: " + rh + "</li>";
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');

        //Weather
        str = "<li> Weather Forecast: " + weather + "</li>";
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');

        //Wind COndition
        str = "<li> Wind Condition: " + wind + "</li>";
        $('ul').append(str);
        $('ul li:last').attr('class', 'list-group-item');

        console.log("Current temperature in " + city + " is: " + temp_f);

      }
    });
  });
});
