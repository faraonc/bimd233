
// Fix the JS so multiple clicks will not add additional list items.  
// Add the “weather” and “wind” indication on to your ajax-wx sample files to callout the type of weather there is and 
// the string used to describe the current wind conditions
// Using the icon set provided by https://www.wunderground.com/weather/api/d/docs?d=resources/icon-sets 
// (Links to an external site.)Links to an external site., choose the icon that best matches the current conditions and 
// apply that to the same list item that contains the text.

// Shorthand for $( document ).ready()
$(function() {
  // weather update button click
  $('button').on('click', function(e) {
  	$(this).remove();
  	$('ul li').each(function() {
  		console.log("this:" + this);
  		$(this).remove();
  	});
  	$.ajax({
  		url: "http://api.wunderground.com/api/7542a9a678392c2f/geolookup/conditions/q/WA/Bothell.json",
  		dataType: "jsonp",
  		success: function(parsed_json) {
  			var city = parsed_json['location']['city'];
  			var state = parsed_json['location']['state'];
  			var temp_f = parsed_json['current_observation']['temp_f'];
  			var rh = parsed_json['current_observation']['relative_humidity'];
  			var weather = parsed_json['current_observation']['weather'];
  			var wind = parsed_json['current_observation']['wind_string'];
  			var time = parsed_json['current_observation']['observation_time_rfc822'];
  			var current_date = new Date(time);
  			var current_hour = current_date.getHours();

  			var weather_icon;
  			if(current_hour > 6 && current_hour < 18){
  				weather_icon = weather.replace(/\s+/g, '').toLowerCase();

  			}
  			else {
  				weather_icon = "nt_" + weather.replace(/\s+/g, '').toLowerCase();
  			}

			//city
			var str = "<li> <img src=\"https://icons.wxug.com/i/c/k/" + weather_icon + ".gif\" height=\"110\" width=\"110\">" + "</li>";
			$('ul').append(str);
			$('ul li:last').attr('class', 'list-group-item text-center');

      //city
      str = "<li> Location : " + city + ", " + state + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');

			//temperature
			str = "<li> Temperature: " + temp_f + " deg F</li>";
			$('ul').append(str);
			$('ul li:last').attr('class', 'list-group-item');

			//humidity
			str = "<li> Relative Humidity: " + rh + "</li>";
			$('ul').append(str);
			$('ul li:last').attr('class', 'list-group-item');

			//Weather
			str = "<li> Weather Forecast: " + weather + "</li>";
			$('ul').append(str);
			$('ul li:last').attr('class', 'list-group-item');

			//Wind COndition
			str = "<li> Wind Condition: " + wind + "</li>";
			$('ul').append(str);
			$('ul li:last').attr('class', 'list-group-item');


			console.log("Current temperature in " + city + " is: " + temp_f);

		}
	});
  });
});


