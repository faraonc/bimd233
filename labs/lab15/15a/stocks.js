"use strict";
/*Using the companies array of objects, use the forEach() 
Array method and a styled button from bootstrap to 
“dump” the data into a striped bootstrap styled table 
output.  (Use your previous labs for inspiration).*/

// Company Name, 
// Market Cap, 
// Sales, Profit,
// and number of employees data.
const NUM_COMPANY = 10;

var names = [
"Microsoft", 
"Symetra Financial",
"Micron Technology",
"F5 Networks",
"Expedia",
"Nautilus",
"Heritage Financial",
"Cascade Microtech",
"Nike",
"Alaska Air Group"
];

var marketCaps = [
"$381.7 B",
"$2.7 B",
"$37.6 B",
"$9.5 B",
"$10.8 B",
"$476 M",
"$531 M",
"$239 M",
"$83.1 B",
"$7.9 B"
];

var sales = [
"$86.8 B",
"$2.2 B",
"$16.4 B",
"$1.7 B",
"$5.8 B",
"$274.4 M",
"$137.6 M",
"$136 M",
"$27.8 B",
"$5.4 B"
];

var profits = [
"$22.1 B",
"$254.4 M",
"$3.0 B",
"$311.2 M",
"$398.1 M",
"$18.8 M",
"$21 M",
"$9.9 M",
"$2.7 B",
"$605 M"
];

var numEmployees = [
"128,000",
"1,400",
"30,400",
"3,834",
"18,210",
"340",
"748",
"449",
"56,500",
"13,952"
];

var companies = [];

for(var i = 0; i < NUM_COMPANY; i++){
	companies.push({
		"name" : names[i],
		"marketCap" : marketCaps[i],
		"sale" : sales[i],
		"profit" : profits[i],
		"numEmployee" : numEmployees[i]
	});

	console.log(companies[i]);
}

var tableRef = document.getElementById("stock-table");


function showData(item, index) {
	var i = 0
	var newRow   = tableRef.insertRow(tableRef.rows.length);
	var newCell  = newRow.insertCell(i);
	newCell.textContent = item['name'];
	i++;
	var newCell  = newRow.insertCell(i);
	newCell.textContent = item['marketCap'];
	i++;
	var newCell  = newRow.insertCell(i);
	newCell.textContent = item['sale'];
	i++;
	var newCell  = newRow.insertCell(i);
	newCell.textContent = item['profit'];
	i++;
	var newCell  = newRow.insertCell(i);
	newCell.textContent = item['numEmployee'];
	i++;
}

function disableButton(){
	var element = document.getElementById("the-button");
	element.removeAttribute("onclick");
}
