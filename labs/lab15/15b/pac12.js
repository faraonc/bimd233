"use strict";

function University(imgLink, rank, name, winConference, lossConference, winOverAll, lossOverAll, lastResult, lastGame) {
	this.imgLink = imgLink;
	this.rank = rank;
	this.name = name;
	this.winConference = winConference;
	this.lossConference = lossConference;
	this.winOverAll = winOverAll;
	this.lossOverAll = lossOverAll;
	this.lastResult = lastResult;
	this.lastGame = lastGame;
}

var uw = new University("http://x.pac-12.com/sites/default/files/styles/thumbnail/public/washington-logo_0__1438812441.png?itok=gDQCuMJ2", 6, "Washington", 7, 1, 10, 1, "W", "44-18 ASU");
var wsu = new University("http://x.pac-12.com/sites/default/files/styles/thumbnail/public/washington-state-logo__1438812470.png?itok=q0ynricm", 22, "Washington State", 7, 1, 8, 3, "L", "24-38 COLO");
var su = new University("http://x.pac-12.com/sites/default/files/styles/thumbnail/public/Stanford_Logo_New_LG__1438812239.png?itok=3CxB0utl", 24, "Stanford", 6, 3, 8, 3, "W", "45-31 CAL");

var cal = new University("http://x.pac-12.com/sites/default/files/styles/thumbnail/public/cal_yellow_lg_2017.png?itok=2nn6ONRS", -1, "California", 2, 6, 4, 7, "L", "31-45 STAN");
var ou = new University("http://x.pac-12.com/sites/default/files/styles/thumbnail/public/oregon-g-logo__1438812094.png?itok=WvbZePjJ", -1, "Oregon", 2, 6, 4, 7, "L", "30-28 UTAH");
var osu = new University("http://x.pac-12.com/sites/default/files/styles/thumbnail/public/oregon-state-logo_0__1438812147.png?itok=4KiO01vm", -1, "Oregon State", 2, 6, 3, 8, "W", "42-17 ARIZ");

var teams = [uw, wsu, su, cal, ou, osu];

console.log(teams);

var tableRef = document.getElementById("game-table");
for(var i = 0; i < teams.length; i++){
	var j = 0;
	var newRow   = tableRef.insertRow(tableRef.rows.length);

	var newCell  = newRow.insertCell(j);
	newCell.innerHTML = "<img src=\"" + teams[i].imgLink + "\">";
	j++;

	var newCell  = newRow.insertCell(j);
	if(teams[i].rank !== -1){
		newCell.innerHTML = "<b>" + teams[i].rank + "</b>" +" " + "<span>" + teams[i].name + "</span>";
	}else{
		newCell.innerHTML = "<span>" + teams[i].name + "</span>";
	}
	j++;

	var newCell  = newRow.insertCell(j);
	newCell.textContent = teams[i].winConference + "-" + teams[i].lossConference;
	j++;

	var newCell  = newRow.insertCell(j);
	newCell.textContent = teams[i].winOverAll + "-" + teams[i].lossOverAll;
	j++;

	var newCell  = newRow.insertCell(j);

	var result = "<b class=\"gray-out\">" + teams[i].lastResult + "</b> "; 
	var game = "<span>" + teams[i].lastGame + "</span>";
	newCell.innerHTML = result + game;

};