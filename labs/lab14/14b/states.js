"use strict";

var cmd = "";
var state = "IDLE";
var options = "Options: run, exit, quit";
do{
	switch (state)  {  
		case "IDLE":     
		{        
			if (cmd === "run") {          
				state = "S1";
				options = "Options: next, skip, prev, exit, quit";         
			}     
		}     
		break;  

		case "S1":    
		{      
			if (cmd === "next") {         
				state = "S2";        
				options = "Options: next, exit, quit";
			} else if (cmd === "skip") {         
				state = "S3";    
				options = "Options: next, home, exit, quit";      
			} else if (cmd === "prev") {    
				state = "S4";   
				options = "Options: next, exit, quit";     
			}    
		}     
		break;

		case "S2":    
		{       
			
			if (cmd === "next") {         
				state = "S3";  
				options = "Options: next, home, exit, quit";      
			} 
		}     
		break;

		case "S3":    
		{       
			if (cmd === "next") {         
				state = "S4";    
				options = "Options: next, exit, quit";    
			} else if (cmd === "home") {         
				state = "IDLE"; 
				options = "Options: run, exit, quit";
			}
		}     
		break;

		case "S4":    
		{       
			if (cmd === "next") {         
				state = "S1";  
				options = "Options: next, skip, prev, exit, quit";       
			} 
		}     
	}
	cmd = prompt("Current state: " + state + "\n\nEnter a command: ", options);
}while(cmd !== "quit" && cmd !== "exit");

