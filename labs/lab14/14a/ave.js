"use strict";

var high = [82, 75, 69, 69, 68];
var low = [55, 52, 52, 48, 51];

function getSum(total, num){
	return total + num;
}

for(var i = 0; i < high.length; i++){
	var lowId = "low" + (i+1);
	var highId = "high" + (i+1);

	document.getElementById(highId).textContent = high[i] + String.fromCharCode(176) + 'F';
	document.getElementById(lowId).textContent = low[i] + String.fromCharCode(176) + 'F';
}

document.getElementById("high-ave").textContent = (high.reduce(getSum)/high.length).toFixed(1) + String.fromCharCode(176) + 'F';
document.getElementById("low-ave").textContent = (low.reduce(getSum)/low.length).toFixed(1) + String.fromCharCode(176) + 'F';
