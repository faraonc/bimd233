var cars = [
	["Subaru", "Legacy", "2018", "$23,608"],
	["Toyota", "Camry", "2018", "$25,202"],
	["Honda", "Accord", "2018", "$22,830"],
	["Nissan", "Sentra", "2017", "$17,068"],
	["Mazda", "Mazda6", "2017", "$26,554"]
];

var tableRef = document.getElementById("car-table");
const HEADER_LENGTH = 4
for(var i = 0; i < cars.length; i++)
{
	// Insert a row in the table at the last row
	var newRow   = tableRef.insertRow(tableRef.rows.length);
	for(var j = 0; j < HEADER_LENGTH; j++)
	{
		// Insert a cell in the row at index 0
		var newCell  = newRow.insertCell(j);
		// Append a text node to the cell
		newCell.textContent = cars[i][j];
	}
};