function calcCircleGeometries(radius) {
	const pi = Math.PI;
	var area = pi * radius * radius;
	var circumference = 2 * pi * radius;
	var diameter = 2 * radius;
	var geometries = [area, circumference, diameter]; 
	return geometries;
}

var radius = [
	(Math.floor(Math.random() * 100) + 1), 
	(Math.floor(Math.random() * 100) + 1), 
	(Math.floor(Math.random() * 100) + 1) 
	];


var result = [];

for(var i = 0; i < radius.length; i++){
	result.push(calcCircleGeometries(radius[i]));
}

for(var i = 0; i < result.length; i++){
	console.log(radius[i]);
	console.log(result[i]);
}

var el = document.getElementById("radius-0")
el.textContent =  "Radius: " + radius[0] + " ---- " + "Area: " + result[0][0] + " ---- " + "Circumference: " + result[0][1] + " ---- " + "Diameter: " + result[0][2];

var el = document.getElementById("radius-1")
el.textContent =  "Radius: " + radius[1] + " ---- " + "Area: " + result[1][0] + " ---- " + "Circumference: " + result[1][1] + " ---- " + "Diameter: " + result[1][2];

var el = document.getElementById("radius-2")
el.textContent =  "Radius: " + radius[2] + " ---- " + "Area: " + result[2][0] + " ---- " + "Circumference: " + result[2][1] + " ---- " + "Diameter: " + result[2][2];