$(document).ready(function() {
	$("li").css("id", "uw");
	const states = ["idle", "gather", "process"];
	const IDLE = 0;
	const GATHER = 1;
	const PROCESS = 2;
	const ENTER = 13;
	var state = states[IDLE];
	console.log('STATE:' + state);
	var words = new Array();

	$("ul").on("mouseover", "li", function() {
		console.log("x:" + $(this).text());
		$(this).attr("id", "uw");
	});

	$("ul").on("mouseleave", "li", function() {
		$(this).attr("id", "uw-gold");
	});

	// reset button click
	$("button").on("click", function(e) {
		//shift the queue of elements
		//to remove each;
		while(words.length > 0){
			var el = words.shift();
			el.parentNode.removeChild(el);
		}
	});

	// keypress
	$("input").on("keypress", function(e) {

		//ascii value
		var code = e.which;

		//char
		var char = String.fromCharCode(code);

		//if code is not enter or string is not empty then switch state to process
		if(code === ENTER && state === states[GATHER] && $('input').val().length >0){
			state = states[PROCESS];
			console.log('STATE:' + state);
		}else if (code === ENTER && state === states[GATHER] && $('input').val().length === 0){
			state = states[IDLE];
			console.log('STATE:' + state);
		}

		

		switch (state) {
			// idle
			case "idle":
				//switch state
				if(code !== ENTER){
					state = states[1];
					console.log('STATE:' + state);
				}
				break;	

			// gather
			case "gather":
				break;

			// process
			case "process":
				//create new element
				var el = document.createElement('li');
				//add bootstrap class
				el.classList.add('list-group-item');
				//change color, not sure why instructor used id in CSS, better to use class
				el.id = "uw-gold";
				//change text
				el.innerText = $('input').val();
				//add the element to the list
				$('.list-group').append(el)
				//add the element to the array for removal later
				words.push(el)
				//clear the text input
				$('input').val("");
				//reset the state to idle
				state = states[IDLE];
				console.log('STATE:' + state);
		}
	});
});
